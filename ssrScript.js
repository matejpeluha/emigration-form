function setPage() {
    setDatePicker();
    editSecondSelect();
}

function setDatePicker() {
    const today = getTodayDate();
    const birthDateInput = document.getElementById("birthDate");

    if(birthDateInput)
        birthDateInput.setAttribute("max", String(today));
}

function getTodayDate(){
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1;
    let yyyy = today.getFullYear();

    if (dd < 10)
        dd = '0' + dd

    if (mm < 10)
        mm = '0' + mm

    today = yyyy + '-' + mm + '-' + dd;

    return today;
}

function firstNameFilledTest(){
    filledInputTest("firstName", "firstNameWarning");
}

function lastNameFilledTest(){
    filledInputTest("lastName", "lastNameWarning");
}

function dateFilledTest(){
    filledInputTest("birthDate", "birthDateWarning");
}

function cityFilledTest(){
    filledInputTest("city", "cityWarning");
}

function streetFilledTest(){
    filledInputTest("street", "streetWarning");
}

function houseNumberTest(){
    const houseNumberId = "houseNumber"
    filledInputTest(houseNumberId, "houseNumberWarning");

    const houseNumberInput = document.getElementById(houseNumberId);

    if(!houseNumberInput)
        return ;

    let houseNumber = houseNumberInput.value;

    if(!houseNumber)
        return ;

    if(houseNumber < 1)
        houseNumber = 1;

    houseNumberInput.value = Math.floor(houseNumber);
}

function filledInputTest(inputId, warningId){
    const input = document.getElementById(inputId);
    const warning = document.getElementById(warningId);

    if(!input || !warning)
        return ;

    if(input.value.trim()) {
        input.style.borderColor = "green";
        warning.style.display = "none";
    }
    else{
        input.style.borderColor = "red";
        warning.style.display = "block";
    }
}


function ageTest(){
    const ageId = "age";
    filledInputTest(ageId, "ageWarning1");

    const ageInput = document.getElementById(ageId);
    const ageWarning = document.getElementById("ageWarning2");
    const dateInput = document.getElementById("birthDate");

    if(!dateInput || !ageInput || !ageWarning)
        return ;

    let age = ageInput.value;
    const date = dateInput.value;
    let dateAge;

    if(!age) {
        ageWarning.style.display = "none";
        return ;
    }

    if(date)
        dateAge = getAge(date);

    if(age < 0)
        age = 0;

    ageInput.value = Math.floor(age);

    if (Number(age) === Number(dateAge)) {
        ageInput.style.borderColor = "green";
        ageWarning.style.display = "none";
    } else {
        ageInput.style.borderColor = "red";
        ageWarning.style.display = "block";
    }
}

function getAge(dateString) {
    const today = new Date();
    const birthDate = new Date(dateString);
    let age = today.getFullYear() - birthDate.getFullYear();
    const m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

function mailTest(){
    const mailId = "mail";
    filledInputTest(mailId, "mailWarning1");

    const mailInput = document.getElementById(mailId);
    const warningInput = document.getElementById("mailWarning2");

    if(!mailInput || !warningInput)
        return ;

    const mail = mailInput.value;

    if(isMailValid(mail)){
        mailInput.style.borderColor = "green";
        warningInput.style.display = "none";
    }
    else if(!mail){
        warningInput.style.display = "none";
    }
    else{
        mailInput.style.borderColor = "red";
        warningInput.style.display = "block";
    }
}

function isMailValid(mail){
    let index;
    if(!mail)
        return false;
    else if(!mail.includes("@"))
        return false;

    const mailNickname =  mail.substr(0, mail.indexOf('@'));
    if(mailNickname.length < 3)
        return false;

    const mailDomains = mail.substr(mail.indexOf('@') + 1, mail.length - 1);
    const listOfDomains = mailDomains.split('.');
    const numberOfDomains = listOfDomains.length;

    if(numberOfDomains < 2)
        return false;

    for(index = 0; index < numberOfDomains; index++) {
        if (listOfDomains[index] === "")
            return false;
    }

    const lastDomainLength = listOfDomains[index - 1].length;
    return !(lastDomainLength < 2 || lastDomainLength > 4);
}

function agreeInfoTest(){
    const checkbox = document.getElementById("agree");
    const label = document.getElementById("agreeLabel");

    if(!checkbox || !label)
        return ;

    if(checkbox.checked)
        label.style.color = "green";
    else
        label.style.color = "red";

}

function manageTextInput() {
    const hiddenInput = document.getElementById("anotherPurpose");
    const anotherPurposeCheckBox = document.getElementById("anotherCheckBox");


    if (!hiddenInput || !anotherPurposeCheckBox)
        return;


    if (anotherPurposeCheckBox.checked) {
    hiddenInput.style.display = "block";
    hiddenInput.disabled = false;
    }
    else {
        hiddenInput.style.display = "none";
        hiddenInput.disabled = true;
    }
}

function emigration(){
    const radioButton = document.querySelector('input[name="block"]:checked');
    const eastForm = document.getElementById("easternBlock");
    const westForm = document.getElementById("westernBlock");

    if(!radioButton || !eastForm || !westForm)
        return ;

    const radioButtonValue = radioButton.value;

    if(radioButtonValue === "Východný blok"){
        eastForm.style.display = "block";
        westForm.style.display = "none";
        eastForm.disabled = false;
        westForm.disabled = true;
    }
    else if(radioButtonValue === "Západný blok"){
        eastForm.style.display = "none";
        westForm.style.display = "block";
        eastForm.disabled = true;
        westForm.disabled = false;
    }
}

function editSecondSelect(){
    const select = document.getElementById("firstSelect");

    if(!select)
        return ;

    const option = select.value;


    if(String(option) === "Centrálne spoločenstvo slobodných štátov")
        editToCentralUnion();
    else if(String(option) === "Rosijský zväz")
        editToRosijaUnion();
}

function editToCentralUnion(){
    const newOptions = ["Preddunajsko", "Zadunajsko"];
    const selectId = "secondSelect";

    editWithNewOptions(newOptions, selectId);
    editThirdSelect();
}

function editToRosijaUnion(){
    const newOptions = ["Ústredie", "Pridružené republiky"];
    const selectId = "secondSelect";

    editWithNewOptions(newOptions, selectId);
    editThirdSelect();
}

function editThirdSelect(){
    const select = document.getElementById("secondSelect");

    if(!select)
        return ;

    const option = select.value;

    if(String(option) === "Preddunajsko")
        editToPreddunajsko();
    else if(String(option) === "Zadunajsko")
        editToZadunajsko();
    else if(String(option) === "Pridružené republiky")
        editToPridruzene();
    else
        editToUstredie();


}

function editToPreddunajsko(){
    const newOptions = ["Juhoslažská spojená republika", "Veľká Uhéria"];
    const selectId = "thirdSelect";

    editWithNewOptions(newOptions, selectId);
}

function editToZadunajsko(){
    const newOptions = ["Čašská republika", "Peľská ľudovodemokratická republika"];
    const selectId = "thirdSelect";

    editWithNewOptions(newOptions, selectId);
}

function editToUstredie(){
    const newOptions = ["Rosijská socialistická republika", "Bielorosijská ľudová republika"];
    const selectId = "thirdSelect";

    editWithNewOptions(newOptions, selectId);
}

function editToPridruzene(){
    const newOptions = ["Okrajunská socialistická družstevná republika", "Karpatská ľudová socialistická republika"];
    const selectId = "thirdSelect";

    editWithNewOptions(newOptions, selectId);
}


function editWithNewOptions(newOptions, selectId){
    const select = document.getElementById(selectId);

    if(!select)
        return ;

    for(let index = 0; index < 2; index++){
        select.options[index].value = newOptions[index];
        select.options[index].innerText = newOptions[index];
        select.options[index].hidden = false;
    }

    select.options[2].selected = true;
}

function validateForm(){
    const firstName = document.getElementById("firstName").value;
    const lastName = document.getElementById("lastName").value;
    const date = document.getElementById("birthDate").value;
    const dateAge = getAge(date);
    const age = document.getElementById("age").value;
    const mail = document.getElementById("mail").value;
    const city = document.getElementById("city").value;
    const street = document.getElementById("street").value;
    const houseNumber = document.getElementById("houseNumber").value;
    const agreeCheckbox = document.getElementById("agree");

    if(!firstName.trim())
        return false;
    else if(!lastName.trim())
        return false;
    else if(!date)
        return false;
    else if(!age)
        return false;
    else if(Number(age) !== Number(dateAge))
        return false;
    else if(!isMailValid(mail))
        return false;
    else if(!city)
        return false;
    else if(!street)
        return false;
    else if(!houseNumber || houseNumber < 1)
        return false;
    else if(!agreeCheckbox.checked)
        return false;

    return true;
}